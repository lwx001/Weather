package cn.lwx.weather;

import android.util.Xml;

//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;

import org.xmlpull.v1.XmlPullParser;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * 解析天气的工具类
 */
public class WeatherService {
    //解析xml文件返回天气信息的集合 [存储解析信息的集合] 接收InputStream流
    public static List<WeatherInfo> getInfosFromXML(InputStream is) throws Exception {
        //1、获取pull解析器
        XmlPullParser parser = Xml.newPullParser();//工具类
        //2、初始化解析器,第一个参数代表包含xml的数据
        parser.setInput(is, "utf-8");//流、编码

        List<WeatherInfo> weatherInfos = null;
        WeatherInfo weatherInfo = null;

        //3、得到当前事件的类型
        int type = parser.getEventType();
        //4、不到文件的结尾,就一直解析。END_DOCUMENT: 文档结束标签
        while (type != XmlPullParser.END_DOCUMENT) {
            //5、具体判断一下解析的是什么标签（开始标签or结束标签）。
            switch (type) {
                //一个节点的开始标签
                case XmlPullParser.START_TAG://代表 解析 开始标签
                    //解析到全局开始的标签 infos 根节点
                    if ("infos".equals(parser.getName())) {//parser.getName(): 拿到当前标签的名字
                        weatherInfos = new ArrayList<WeatherInfo>();//创建集合对象
                    } else if ("city".equals(parser.getName())) {
                        weatherInfo = new WeatherInfo();//初始化weatherInfo的信息
                        String idStr = parser.getAttributeValue(0);//获取weather1.xml文件中city的id值
                        weatherInfo.setId(idStr);
                    } else if ("temp".equals(parser.getName())) {
                        //parset.nextText()得到该tag节点中的内容
                        String temp = parser.nextText();
                        weatherInfo.setTemp(temp);
                    } else if ("weather".equals(parser.getName())) {
                        String weather = parser.nextText();
                        weatherInfo.setWeather(weather);
                    } else if ("name".equals(parser.getName())) {
                        String name = parser.nextText();
                        weatherInfo.setName(name);
                    } else if ("pm".equals(parser.getName())) {
                        String pm = parser.nextText();
                        weatherInfo.setPm(pm);
                    } else if ("wind".equals(parser.getName())) {
                        String wind = parser.nextText();
                        weatherInfo.setWind(wind);
                    }
                    break;
                //一个节点结束的标签
                case XmlPullParser.END_TAG:
                    //一个城市的信息处理完毕，city的结束标签
                    if ("city".equals(parser.getName())) {
                        weatherInfos.add(weatherInfo);//把bean对象加到集合中
                        weatherInfo = null;
                    }
                    break;
            }
            type = parser.next();//赋值：解析后的事件类型
        }
        return weatherInfos;
    }

//    //解析json文件返回天气信息的集合
//    public static List<WeatherInfo> getInfosFromJson(InputStream is)
//            throws IOException {
//        byte[] buffer = new byte[is.available()];
//        is.read(buffer);
//        String json = new String(buffer, "utf-8");
//        //使用gson库解析JSON数据
//        Gson gson = new Gson();
//        Type listType = new TypeToken<List<WeatherInfo>>() {
//        }.getType();
//        List<WeatherInfo> weatherInfos = gson.fromJson(json, listType);
//        return weatherInfos;
//    }

}
