package cn.lwx.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvCity;
    private TextView tvWeather;
    private TextView tvTemp;
    private TextView tvWind;
    private TextView tvPm;
    private ImageView ivIcon;
    private List<Map<String, String>> list;//存储天气信息的集合
    private Map<String, String> map;
    private String temp, weather, name, pm, wind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //raw中的文件会被自动解析、编译。通过raw目录找到相应的文件

        //1、初始化文本控件
        initView();

        //2、读取解析weather1.xml文件信息
        InputStream inputStream = getResources().openRawResource(R.raw.weather1);//getResources()得到资源文件
        try {
            List<WeatherInfo> weatherInfos = WeatherService.getInfosFromXML(inputStream);//调用工具类[传参：inputStream流]
            list = new ArrayList<Map<String, String>>();//存储天气信息的集合
            //循环读取weatherInfos中的每一条数据
            for (WeatherInfo info : weatherInfos) {
                map = new HashMap<String, String>();
                map.put("temp", info.getTemp());
                map.put("weather", info.getWeather());
                map.put("name", info.getName());
                map.put("pm", info.getPm());
                map.put("wind", info.getWind());
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //自定义getMap()方法，显示天气信息到文本控件中，默认显示北京的天气
        getMap(1, R.drawable.sun);
    }

    private void initView() {
        tvCity = (TextView) findViewById(R.id.tv_city);
        tvWeather = (TextView) findViewById(R.id.tv_weather);
        tvTemp = (TextView) findViewById(R.id.tv_temp);
        tvWind = (TextView) findViewById(R.id.tv_wind);
        tvPm = (TextView) findViewById(R.id.tv_pm);
        ivIcon = (ImageView) findViewById(R.id.iv_icon);//展示图片
        findViewById(R.id.btn_sh).setOnClickListener(this);//设置点击
        findViewById(R.id.btn_bj).setOnClickListener(this);
        findViewById(R.id.btn_gz).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sh://点击上海
                getMap(0, R.drawable.cloud_sun);
                break;
            case R.id.btn_bj://点击北京
                getMap(1, R.drawable.sun);
                break;
            case R.id.btn_gz://点击广州
                getMap(2, R.drawable.clouds);
                break;
        }
    }

    //自定义getMap()方法,将城市天气信息分条展示到界面上,默认显示北京天气
    private void getMap(int number, int iconNumber) {
        Map<String, String> cityMap = list.get(number);
        temp = cityMap.get("temp");
        weather = cityMap.get("weather");
        name = cityMap.get("name");
        pm = cityMap.get("pm");
        wind = cityMap.get("wind");
        tvCity.setText(name);
        tvWeather.setText(weather);
        tvTemp.setText("" + temp);
        tvWind.setText("风力  : " + wind);
        tvPm.setText("pm: " + pm);
        ivIcon.setImageResource(iconNumber);
    }
}
